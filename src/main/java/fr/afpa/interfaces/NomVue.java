package fr.afpa.interfaces;
/**
 * 
 * @author sofiane tayeb
 *
 *la class de stockage des urls de l'application
 */
public interface NomVue {
	public final String PAGE_INSCRIPTION="/WEB-INF/utilisateur/inscription.html";
	public final String PAGE_CONNEXION="/WEB-INF/utilisateur/connexion.html";
	public final String PAGE_ACCUEIL="/WEB-INF/utilisateur/accueil.jsp";
	public final String PAGE_POSTER="/WEB-INF/annonce/poster.jsp";
	public final String PAGE_LIST_ANNONCE="/WEB-INF/annonce/consultAnnonce.jsp";
	public final String PAGE_MES_ANNONCE="/WEB-INF/annonce/mesAnnonce.jsp";
	public final String PAGE_MODIF_ANNONCE="/WEB-INF/annonce/modifierAnnonce.jsp";
	public final String ADMIN_CONNEXION="/WEB-INF/admin/connect-admin.jsp";
	public final String PAGE_ACCUEIL_ADMIN="/WEB-INF/admin/accueilAdmin.jsp";
	public final String PAGE_LIST_UTILISATEUR = "/WEB-INF/admin/listeUtilisateur.jsp";
	public final String PAGE_GESTION_ANNONCE = "/WEB-INF/admin/gestionAnnonce.jsp";

	


	
}
