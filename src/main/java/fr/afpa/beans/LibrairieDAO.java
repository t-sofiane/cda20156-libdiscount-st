package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "librairie")
@Table(name = "librairie")

/**
 * 
 * @author sofiane Tayeb
 *
 */
public class LibrairieDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "librairie_id_gen")
	@SequenceGenerator(name="librairie_id_gen", sequenceName = "seq_librairie", allocationSize = 1)
	@Column(name="librairie_id")
	private int idLibrairie;
	@Column(name="librairie_numVoie")
	private String numVoie;
	@Column(name="librairie_voie")
	private String voie;
	@Column(name="librairie_codePostal")
	private String codePostal;
	@Column(name="librairie_commune")
	private String commune;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy="librairie"  )
	private UtilisateurDAO userList;
	
}
