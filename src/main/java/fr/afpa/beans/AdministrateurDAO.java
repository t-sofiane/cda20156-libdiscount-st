package fr.afpa.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity(name = "administrateur")
@NamedQuery(name = "findByLogin", query = "select admin from administrateur admin where admin.login = :login and admin.passWord= :pass")

/**	
 * 
 * @author sofiane Tayeb
 *
 */
public class AdministrateurDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "admin_id_gen")
	@SequenceGenerator(name="admin_id_gen", sequenceName = "seq_admin", allocationSize = 1)
	@Column(name="num_admin")
	private int idAdmin;
	@Column(name="login")
	private String login;
	@Column(name="pass_word")
	private String passWord;
}
