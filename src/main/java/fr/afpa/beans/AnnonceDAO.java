package fr.afpa.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="annonce")
@Table(name="annonce")
@NamedQuery(name = "findByUser", query = "select a from annonce a where a.archivage=: archive and user.idUtilisateur = :idUser") 
@NamedQuery(name = "findById", query = "select a from annonce a where a.archivage=: archive and a.idAnnonce = :idAnnonce") 
@NamedQuery(name = "findByTitle", query = "select a from annonce a where a.archivage=: archive and a.titreAnnonce = :titre") 
@NamedQuery(name = "findByLivel", query = "select a from annonce a where a.archivage=: archive and a.nivScolaire = :niveau") 
@NamedQuery(name = "findByVille", query = "select a from annonce a where a.archivage=: archive and user.librairie.commune = :ville") 

/**
 * 
 * @author sofiane Tayeb
 * la class AnnonceDAO 
 */
public class AnnonceDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "annonce_id_gen")
	@SequenceGenerator(name="annonce_id_gen", sequenceName = "seq_annonce" , allocationSize = 1)
	@Column(name="annonce_id")
	private int idAnnonce;
	@Column(name="titre_Annonce")	
	private String titreAnnonce;
	@Column(name="niv_Scolaire")	
	private String nivScolaire;
	@Column(name="isbn")	
	private String isbn;
	@Column(name="date_Edition")	
	private String dateEdition;
	@Column(name="maison_Edition")	
	private String maisonEdition;
	@Column(name="prix_Unit")	
	private float prixUnit;
	@Column(name="quantite")	
	private int quantite;
	@Column(name="archivage")	
	private boolean archivage;
	@Column(name="photo")	
	private String photo;
	
	@ManyToOne ()
	@JoinColumn (name = "user_id")
	private UtilisateurDAO user;

}
