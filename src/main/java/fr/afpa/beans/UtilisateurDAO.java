package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity(name = "utilisateur")
@NamedQuery(name = "findByMail", query = "select user from utilisateur user where user.active= :isActive and user.mail = :mail "
		+ "and user.passWord = :pass") 
@NamedQuery(name = "findByIdUser", query = "select user from utilisateur user where user.idUtilisateur= :idUser")

/**
 * 
 * @author sofiane Tayeb
 *
 * la classe Utilisateur
 */
public class UtilisateurDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_gen")
	@SequenceGenerator(name="user_id_gen", sequenceName = "seq_user", allocationSize = 1)
	@Column(name="user_id")
	private int idUtilisateur;
	@Column(name="user_nom")
	private String nom;
	@Column(name="user_prenom")
	private String prenom;
	@Column(name="user_mail" , unique = true)
	private String mail;
	@Column(name="user_telephone")
	private String telephone;
	@Column(name="user_passWord")
	private String passWord;
	@Column(name="user_type", columnDefinition="int default 0")
	private int type;
	@Column(name="user_active", columnDefinition="boolean default true")
	private boolean active= true;
	
	@OneToOne (cascade= {CascadeType.ALL}  )
	@JoinColumn(name="librairie_id")
	private LibrairieDAO librairie;

	@OneToMany(cascade= {CascadeType.ALL} )
	//@JoinColumn(name = "idAnnonce")
	private List <AnnonceDAO> listAnnonce;
	
}
