package fr.afpa.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAnnonce;

/**
 * Servlet implementation class ConsultAnnonce
 */
public class ConsultAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		session.getAttribute("utilisateur");
		 //int idUserSession = ((UtilisateurDAO) session.getAttribute("utilisateur")).getIdUtilisateur();
		 String typeRec = request.getParameter("type");
		 String rechercheRec = request.getParameter("recherche");
		 
		 if (typeRec==null || rechercheRec==null) {
			//request.setAttribute("idUser", idUserSession);
			GestionAnnonce ga= new GestionAnnonce();
			request.setAttribute("listAnnonce", ga.listAnnonce());
			request.getRequestDispatcher(NomVue.PAGE_LIST_ANNONCE).forward(request, response);
		 
		 }else if("titre".equals(typeRec)) {
				GestionAnnonce ga= new GestionAnnonce();
				request.setAttribute("listAnnonce", ga.recherchParTitre(rechercheRec));
				request.getRequestDispatcher(NomVue.PAGE_LIST_ANNONCE).forward(request, response);

		 }else if("niveau".equals(typeRec)) {
			 GestionAnnonce ga= new GestionAnnonce();
			 request.setAttribute("listAnnonce", ga.recherchParNiveau(rechercheRec));
			 request.getRequestDispatcher(NomVue.PAGE_LIST_ANNONCE).forward(request, response);
		 }else if("ville".equals(typeRec)) {
			 GestionAnnonce ga= new GestionAnnonce();
			 request.setAttribute("listAnnonce", ga.recherchParVille(rechercheRec));
			 request.getRequestDispatcher(NomVue.PAGE_LIST_ANNONCE).forward(request, response);
		 }
	}

}
