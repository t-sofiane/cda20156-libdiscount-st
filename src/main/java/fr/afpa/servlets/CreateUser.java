package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.LibrairieDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.model.services.GestionUtilisateur;

/**
 * Servlet implementation class CreateUser
 */
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
//		LocalDate dateRec=LocalDate.parse(request.getParameter("dateDN"), formatter); 

//		User		
		String nomRec = request.getParameter("nom");
		String prenomRec = request.getParameter("prenom");
		String emailRec = request.getParameter("email");
		String passWordRec= request.getParameter("motdepasse");
		String telephoneRec= request.getParameter("telephone");
//		librairie
		String numVoieRec = request.getParameter("numVoie");
		String voieRec = request.getParameter("voie");
		String codePostalRec= request.getParameter("codePostal");
		String communeRec= request.getParameter("commune");

		
		// instancier une personne avec les infos r�cup�r�es
		LibrairieDAO librairie= new LibrairieDAO();
		librairie.setNumVoie(numVoieRec);
		librairie.setVoie(voieRec);
		librairie.setCodePostal(codePostalRec);
		librairie.setCommune(communeRec);
		
		UtilisateurDAO user= new UtilisateurDAO();
		request.setAttribute("utilisateur", user);
		user.setNom(nomRec);
		user.setPrenom(prenomRec);
		user.setMail(emailRec);
		user.setPassWord( passWordRec);
		user.setTelephone(telephoneRec);
		user.setLibrairie(librairie);
		// instancier GestionUtilisateur et appeler la m�thode d'insersion
		GestionUtilisateur insert= new GestionUtilisateur();
		boolean resultAjout=insert.insertUser(request);
		
		// v�rifier le resultat d'ajout de l'utilisateur et la redirection
		if(resultAjout) {
			request.setAttribute("result", resultAjout);
		}else {
			request.setAttribute("result", resultAjout);
			request.setAttribute("utilisateur", null);
		}
		RequestDispatcher dispatch= request.getRequestDispatcher("ajoutSucces.jsp");
		dispatch.forward(request, response);
	}
}
