package fr.afpa.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAnnonce;

/**
 * Servlet implementation class AfficherMesAnnonce
 */
public class AfficherMesAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.getAttribute("utilisateur");
		 int idUserSession = ((UtilisateurDAO) session.getAttribute("utilisateur")).getIdUtilisateur();
		 
			//request.setAttribute("idUser", idUserSession);
			GestionAnnonce ga= new GestionAnnonce();
			request.setAttribute("listAnnonce", ga.mesAnnonce(idUserSession));
			request.getRequestDispatcher(NomVue.PAGE_MES_ANNONCE).forward(request, response);
	}


}
