package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAnnonce;

/**
 * Servlet implementation class SupprimerAnnonce
 */
public class SupprimerAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.getAttribute("utilisateur");
		session.getAttribute("administrateur");
		RequestDispatcher disp = null;
		String nextVue ="";
		int idAnnonce = Integer.parseInt(request.getParameter("num"));		
		request.setAttribute("idAnonce", idAnnonce);
		GestionAnnonce ga= new GestionAnnonce();
		ga.deleteAnnonce(idAnnonce);
		if(session.getAttribute("utilisateur")!=null) {
			RequestDispatcher dispatch= request.getRequestDispatcher(NomVue.PAGE_ACCUEIL);
			dispatch.forward(request, response);
		} else if(session.getAttribute("administrateur")!=null) {
			RequestDispatcher dispatch= request.getRequestDispatcher(NomVue.PAGE_ACCUEIL_ADMIN);
			dispatch.forward(request, response);
		}
		
		
	}

}
