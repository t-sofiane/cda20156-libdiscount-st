package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.AnnonceDAO;
import fr.afpa.beans.LibrairieDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAnnonce;
import fr.afpa.model.services.GestionUtilisateur;

/**
 * Servlet implementation class CreerAnnonce
 */
public class CreerAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String titreRec = request.getParameter("titre");
		String nivScolaireRec = request.getParameter("nivScolaire");
		String isbnRec = request.getParameter("isbn");
		String editDateWordRec= request.getParameter("editDate");
		String maisonEditRec= request.getParameter("maisonEdit");
		String id_userRec = request.getParameter("utilisateur.idUtilisateur");
		float prixUnitRec = Float.parseFloat(request.getParameter("prixUnit")) ;
		int quantiteRec = Integer.parseInt(request.getParameter("quantite"));
		HttpSession session = request.getSession();
		session.getAttribute("utilisateur");
		int idUserRec = ((UtilisateurDAO) session.getAttribute("utilisateur")).getIdUtilisateur();
		
		// instancier une annonce avec les infos r�cup�r�es
		
		
		AnnonceDAO annonce= new AnnonceDAO();
		UtilisateurDAO user= new UtilisateurDAO();
		user.setIdUtilisateur(idUserRec);
		annonce.setTitreAnnonce(titreRec);
		annonce.setNivScolaire(nivScolaireRec);
		annonce.setIsbn(isbnRec);
		annonce.setDateEdition(editDateWordRec);
		annonce.setPrixUnit(prixUnitRec);
		annonce.setQuantite(quantiteRec);
		annonce.setMaisonEdition(maisonEditRec);
		annonce.setUser(user);
		
		request.setAttribute("annonce", annonce);
				
		// instancier GestionUtilisateur et appeler la m�thode d'insersion
		GestionAnnonce insert= new GestionAnnonce();
		boolean resultAjout=insert.insertAnnonce(request);
		
		// v�rifier le resultat d'ajout de l'utilisateur et la redirection
		if(resultAjout) {
			request.setAttribute("result", resultAjout);
		}else {
			request.setAttribute("result", resultAjout);
			request.setAttribute("annonce", null);
		}
		RequestDispatcher dispatch= request.getRequestDispatcher(NomVue.PAGE_ACCUEIL);
		dispatch.forward(request, response);
	}
}

