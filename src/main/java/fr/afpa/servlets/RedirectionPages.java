package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.interfaces.NomVue;

/**
 * Servlet implementation class RedirectionPages
 */
public class RedirectionPages extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher disp = null;
		String nextVue ="";
		String pageDemande = request.getParameter("param");

		if(pageDemande.contentEquals("connexion")) {
			nextVue = NomVue.PAGE_CONNEXION;
		}
		else if(pageDemande.contentEquals("home")) {
			nextVue = NomVue.PAGE_ACCUEIL;
		}else if(pageDemande.contentEquals("inscription")) {
			nextVue = NomVue.PAGE_INSCRIPTION;
		}else if(pageDemande.contentEquals("poster")) {
			nextVue = NomVue.PAGE_POSTER;
		}else if(pageDemande.contentEquals("afficher-annonces")) {
			nextVue = NomVue.PAGE_LIST_ANNONCE;
		}else if(pageDemande.contentEquals("mes-annonces")) {
			nextVue = NomVue.PAGE_MES_ANNONCE;
		}else if(pageDemande.contentEquals("modifier-annonce")) {
			nextVue = NomVue.PAGE_MODIF_ANNONCE;
		}else if(pageDemande.contentEquals("deconnect")) {
			HttpSession session = request.getSession();
			session.getAttribute("utilisateur");
		    //System.out.println(((UtilisateurDAO) session.getAttribute("utilisateur")).getNom()); 
			session.invalidate();
			nextVue = "index.jsp";
		}else {
			System.out.println("ERRRRRRRRROOOOOOOOOORRRRRRRRR !!!!!!");
			nextVue = "index.jsp";
		}
		
		disp = request.getRequestDispatcher(nextVue);
		disp.forward(request, response);
		
	}
		
	}


