package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAdministrateur;
import fr.afpa.model.services.GestionUtilisateur;

/**
 * Servlet implementation class ConnexionAdmin
 */
public class ConnexionAdmin extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// R�cup�ration des informations saisies par l'utilisateur
		String loginForm = request.getParameter("loginAdmin");
		String passForm = request.getParameter("passAdmin");
		//System.out.println(request.getParameter("url"));
		request.setAttribute("login", loginForm);
		request.setAttribute("passWord", passForm);

		GestionAdministrateur ga = new GestionAdministrateur();
		ga.checkConnexionAdmin(request);
		RequestDispatcher disp = null;
		String nextVue ="index.jsp";
		
		if(ga.checkConnexionAdmin(request)!=null) {
	        HttpSession session = request.getSession();
	        session.setAttribute("administrateur", ga.checkConnexionAdmin(request));
			//request.setAttribute("user", gu.checkConnexion(request));
			nextVue = NomVue.PAGE_ACCUEIL_ADMIN;
		}
		else {
			System.out.println("ERROR");
			nextVue = "index.jsp";
		}
		
		disp = request.getRequestDispatcher(nextVue);
		disp.forward(request, response);
	}

}
