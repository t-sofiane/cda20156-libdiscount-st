package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.AnnonceDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAnnonce;

/**
 * Servlet implementation class AnnonceAModifier
 */
public class AnnonceAModifier extends HttpServlet {
	private static final long serialVersionUID = 1L;
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher disp = null;
		String nextVue ="";
		int idAnnonce = Integer.parseInt(request.getParameter("num"));		
		request.setAttribute("idAnonce", idAnnonce);
		GestionAnnonce ga= new GestionAnnonce();
		request.setAttribute("annonce", ga.recupAnnonce(idAnnonce));
		request.getRequestDispatcher(NomVue.PAGE_MODIF_ANNONCE).forward(request, response);
	}

}


