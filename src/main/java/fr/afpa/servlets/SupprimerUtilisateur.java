package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAdministrateur;
import fr.afpa.model.services.GestionAnnonce;

/**
 * Servlet implementation class SupprimerUtilisateur
 */
public class SupprimerUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher disp = null;
		String nextVue ="";
		int idUser = Integer.parseInt(request.getParameter("num"));		
		request.setAttribute("idUser", idUser);
		GestionAdministrateur ga= new GestionAdministrateur();
		ga.deleteUser(idUser);
		RequestDispatcher dispatch= request.getRequestDispatcher(NomVue.PAGE_ACCUEIL_ADMIN);
		dispatch.forward(request, response);
	}

	

}
