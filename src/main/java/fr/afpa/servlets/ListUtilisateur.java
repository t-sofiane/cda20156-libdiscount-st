package fr.afpa.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.interfaces.NomVue;
import fr.afpa.model.services.GestionAdministrateur;
import fr.afpa.model.services.GestionAnnonce;

/**
 * Servlet implementation class ListUtilisateur
 */
public class ListUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.getAttribute("administrateur");
		 
			//request.setAttribute("idUser", idUserSession);
			GestionAdministrateur ga= new GestionAdministrateur();
			request.setAttribute("listUtilisateur", ga.listUtilisateur());
			request.getRequestDispatcher(NomVue.PAGE_LIST_UTILISATEUR).forward(request, response);
		 
	}

}
