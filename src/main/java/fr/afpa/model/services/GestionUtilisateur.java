package fr.afpa.model.services;

import javax.servlet.http.HttpServletRequest;

import fr.afpa.beans.AdministrateurDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.dao.services.GestionUtilisateurDAO;
/**
 * 
 * @author sofiane tayeb
 *
 *la class GestionUtilisateur
 */
public class GestionUtilisateur {

	/**
	 * 
	 * @param request
	 * @return
	 */
	public boolean insertUser(HttpServletRequest request) {
		UtilisateurDAO user= (UtilisateurDAO) request.getAttribute("utilisateur");
		GestionUtilisateurDAO gu= new GestionUtilisateurDAO();
		gu.insertUtilisateur(user);		
		
		return true;
	}
	/**
	 * 
	 * @param request
	 * @return
	 */
	public UtilisateurDAO checkConnexion(HttpServletRequest request) {
		String login = (String) request.getAttribute("login");
		String pass = (String) request.getAttribute("passWord");
		GestionUtilisateurDAO gu= new GestionUtilisateurDAO();
		return gu.checkConnexionDAO(login, pass);

	}

}
