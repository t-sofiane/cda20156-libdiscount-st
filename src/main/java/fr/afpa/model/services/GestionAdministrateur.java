package fr.afpa.model.services;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import fr.afpa.beans.AdministrateurDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.dao.services.GestionAdministrateurDAO;
import fr.afpa.dao.services.GestionAnnonceDAO;
import fr.afpa.dao.services.GestionUtilisateurDAO;
/**
 * 
 * @author sofiane tayeb
 *
 *la class GestionAdministrateur
 */
public class GestionAdministrateur {
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public AdministrateurDAO checkConnexionAdmin(HttpServletRequest request) {
		String login = (String) request.getAttribute("login");
		String pass = (String) request.getAttribute("passWord");

		GestionAdministrateurDAO ga= new GestionAdministrateurDAO();
		return ga.checkConnexionAdminDAO(login, pass);
		
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<UtilisateurDAO> listUtilisateur() {
		GestionAdministrateurDAO ga= new GestionAdministrateurDAO();
		return ga.listUtilisateurDAO();
	}
	
	/**
	 * 
	 * @param idUser
	 */
	public void deleteUser(int idUser) {
		GestionAdministrateurDAO ga= new GestionAdministrateurDAO();
		ga.deleteUserDAO(idUser);
	}


}
