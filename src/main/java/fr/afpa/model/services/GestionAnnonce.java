package fr.afpa.model.services;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import fr.afpa.beans.AnnonceDAO;
import fr.afpa.dao.services.GestionAnnonceDAO;

/**
 * 
 * @author sofiane tayeb
 *
 *la class GestionAnnonce
 */
public class GestionAnnonce {
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public boolean insertAnnonce(HttpServletRequest request) {
		AnnonceDAO annonce= (AnnonceDAO) request.getAttribute("annonce");
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		ga.insertAnnonceDAO(annonce);		
		
		return true;
	}
	
	/**
	 * 
	 * @param idAnonce
	 * @return
	 */
	public boolean deleteAnnonce(int idAnonce) {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		ga.deleteAnnonceDAO(idAnonce);		
		
		return true;
		
	}
	/**
	 * 
	 * @param idAnonce
	 * @return
	 */
	public AnnonceDAO recupAnnonce(int idAnonce) {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		return ga.recupAnnonceDAO(idAnonce);		
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList<AnnonceDAO> listAnnonce() {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		return ga.listAnnonceDAO();
		
	}
	/**
	 * 
	 * @param idUser
	 * @return
	 */
	public ArrayList<AnnonceDAO> mesAnnonce(int idUser) {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		return ga.mesAnnonceDAO(idUser);
		
	}
	/**
	 * 
	 * @param annonce
	 * @return
	 */
	public boolean modifAnnonce(AnnonceDAO annonce) {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		ga.modifAnnonceDAO(annonce);
			return true;
		
	}
	/**
	 * 
	 * @param rechercheRec
	 * @return
	 */
	public ArrayList<AnnonceDAO> recherchParTitre(String rechercheRec) {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		return ga.recherchParTitreDAO(rechercheRec);
		
	}
	/**
	 * 
	 * @param rechercheRec
	 * @return
	 */
	public ArrayList<AnnonceDAO> recherchParNiveau(String rechercheRec) {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		return ga.recherchParNiveauDAO(rechercheRec);
	}
	/**
	 * 
	 * @param rechercheRec
	 * @return
	 */
	public ArrayList<AnnonceDAO> recherchParVille(String rechercheRec) {
		GestionAnnonceDAO ga= new GestionAnnonceDAO();
		return ga.recherchParVilleDAO(rechercheRec);
	}
}
