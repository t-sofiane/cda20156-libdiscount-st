package fr.afpa.dao.services;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.AdministrateurDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.session.HibernateSession;

/**
 * 
 * @author sofiane tayeb
 *
 *la class GestionUtilisateurDAO pour communiquer avec la bdd
 */
public class GestionUtilisateurDAO {

	/**
	 * 
	 * @param user
	 */
	public void insertUtilisateur(UtilisateurDAO user) {
				
		Session s = HibernateSession.getSession();
		Transaction tx = s.beginTransaction();
				
		s.save(user);
		tx.commit();
		s.close();
	}
	/**
	 * class de verification de connexion
	 * @param mail
	 * @param passWord
	 * @return
	 */
	public UtilisateurDAO checkConnexionDAO(String mail, String passWord) {
		
		Session s = HibernateSession.getSession();
		Transaction tx = s.beginTransaction();

		Query query = s.getNamedQuery("findByMail");
		query.setParameter("mail", mail);
		query.setParameter("pass", passWord);
		query.setParameter("isActive", true);
		
		if(!query.getResultList().isEmpty()) {
			UtilisateurDAO user= (UtilisateurDAO) query.getSingleResult();
			return user;
		}
			return null;
	}

}
