package fr.afpa.dao.services;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.AdministrateurDAO;
import fr.afpa.beans.AnnonceDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.session.HibernateSession;
/**
 * 
 * @author sofiane tayeb
 *
 *la class GestionAdministrateurDAO pour communiquer avec la bdd
 */
public class GestionAdministrateurDAO {
	/**
	 * class de vérification de connexion 
	 * @param login
	 * @param passWord
	 * @return
	 */
	public AdministrateurDAO checkConnexionAdminDAO(String login, String passWord) {
		Session s = HibernateSession.getSession();
		Transaction tx = s.beginTransaction();

		Query query = s.getNamedQuery("findByLogin");
		query.setParameter("login", login);
		query.setParameter("pass", passWord);
		
		if(!query.getResultList().isEmpty()) {
			AdministrateurDAO admin= (AdministrateurDAO) query.getSingleResult();
			return admin;
		}
			return null;
	}
	/**
	 * class pour récupérer les utilisateurs actif de l'application
	 * @return la liste des utilisateur 
	 */
	public ArrayList<UtilisateurDAO> listUtilisateurDAO() {
		Session s = HibernateSession.getSession();
		Transaction tx = s.beginTransaction();

		Query query = s.createQuery("from utilisateur where active= :active");
		query.setParameter("active", true);
		ArrayList<UtilisateurDAO> listUser =  (ArrayList<UtilisateurDAO>) query.getResultList();
		s.close();
		return listUser;
	}
	
	/**
	 * class de suppression d'utilisateur
	 * @param idUser
	 */
	
	public void deleteUserDAO(int idUser) {
		
		Session s = HibernateSession.getSession();
		Transaction tx = s.beginTransaction();
		Query query = s.getNamedQuery("findByIdUser");
		query.setParameter("idUser", idUser);
	
		UtilisateurDAO user= (UtilisateurDAO) query.getSingleResult();
		user.setActive(false);
		s.update(user);
		tx.commit();
		s.close();
	}
			
}
