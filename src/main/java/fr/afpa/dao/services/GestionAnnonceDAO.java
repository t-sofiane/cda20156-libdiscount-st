package fr.afpa.dao.services;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import fr.afpa.beans.AnnonceDAO;
import fr.afpa.beans.UtilisateurDAO;
import fr.afpa.session.HibernateSession;

/**
 * 
 * @author sofiane tayeb
 *
 *la class GestionAnnonceDAO pour communiquer avec la bdd
 */

public class GestionAnnonceDAO {
	private Session s ;
	private Transaction tx;
	
	/**
	 * la class insertAnnonceDAO pour inserrer une annonce
	 * @param annonce
	 */
	public void insertAnnonceDAO(AnnonceDAO annonce) {
		
		s = HibernateSession.getSession();
		tx = s.beginTransaction();				
		s.save(annonce);
		tx.commit();
		s.close();
	}
	/**
	 * la class modifAnnonceDAO pour modifier une annonce
	 * @param annonce
	 * @return
	 */
	
	public boolean modifAnnonceDAO(AnnonceDAO annonce) {
		s = HibernateSession.getSession();
		tx = s.beginTransaction();
		s.delete(annonce);
		tx.commit();
		s.close();
		return true;

	}
	/**
	 * class deleteAnnonceDAO pour supprimer une annonce
	 * @param idAnonce
	 */
	public void deleteAnnonceDAO(int idAnonce) {
		
		s = HibernateSession.getSession();
		tx = s.beginTransaction();
		Query query = s.getNamedQuery("findById");
		query.setParameter("idAnnonce", idAnonce);
		query.setParameter("archive", false);
		
		if(!query.getResultList().isEmpty()) {

		AnnonceDAO annonce= (AnnonceDAO) query.getSingleResult();
		annonce.setArchivage(true);
		s.update(annonce);
		tx.commit();
		}
		s.close();
	}
	
	/**
	 * la class recupAnnonceDAO pour récupper une annonce
	 * @param idAnonce
	 * @return une annonce
	 */
	public AnnonceDAO recupAnnonceDAO(int idAnonce) {
		
		s = HibernateSession.getSession();
		tx = s.beginTransaction();
		Query query = s.getNamedQuery("findById");
		query.setParameter("idAnnonce", idAnonce);
		query.setParameter("archive", false);
		
		if(!query.getResultList().isEmpty()) {
			AnnonceDAO annonce= (AnnonceDAO) query.getSingleResult();
			return annonce;

		}
		s.close();

		return null;
	}
	/**
	 * class listAnnonceDAO  pour récupérer la liste des annonces
	 * @return
	 */
	public ArrayList<AnnonceDAO> listAnnonceDAO() {
		
		s = HibernateSession.getSession();
		tx = s.beginTransaction();

		//Query query = s.getNamedQuery("findByUser");
		Query query = s.createQuery("from annonce where archivage= :archive");
		query.setParameter("archive", false);
		ArrayList<AnnonceDAO> listAnnonce =  (ArrayList<AnnonceDAO>) query.getResultList();
		s.close();
		return listAnnonce;
	}
	
	public ArrayList<AnnonceDAO> mesAnnonceDAO(int idUtisateur) {
	
	s = HibernateSession.getSession();
	tx = s.beginTransaction();

	Query query = s.getNamedQuery("findByUser");
	query.setParameter("idUser", idUtisateur);
	query.setParameter("archive", false);

	ArrayList<AnnonceDAO> listAnnonce =  (ArrayList<AnnonceDAO>) query.getResultList();
	s.close();
	return listAnnonce;
	}
	
	/**
	 * 
	 * @param rechercheRec
	 * @return
	 */
	public ArrayList<AnnonceDAO> recherchParTitreDAO(String rechercheRec) {
		s = HibernateSession.getSession();
		tx = s.beginTransaction();

		Query query = s.getNamedQuery("findByTitle");
		query.setParameter("titre", rechercheRec);
		query.setParameter("archive", false);
		ArrayList<AnnonceDAO> listAnnonce =  (ArrayList<AnnonceDAO>) query.getResultList();
		s.close();
		return listAnnonce;
		
	}
	/**
	 * 
	 * @param rechercheRec
	 * @return
	 */
	public ArrayList<AnnonceDAO> recherchParNiveauDAO(String rechercheRec) {
		s = HibernateSession.getSession();
		tx = s.beginTransaction();

		Query query = s.getNamedQuery("findByLivel");
		query.setParameter("niveau", rechercheRec);
		query.setParameter("archive", false);
		ArrayList<AnnonceDAO> listAnnonce =  (ArrayList<AnnonceDAO>) query.getResultList();
		s.close();
		return listAnnonce;
	}
	/**
	 * 
	 * @param rechercheRec
	 * @return
	 */
	public ArrayList<AnnonceDAO> recherchParVilleDAO(String rechercheRec) {
		s = HibernateSession.getSession();
		tx = s.beginTransaction();

		Query query = s.getNamedQuery("findByVille");
		query.setParameter("ville", rechercheRec);
		query.setParameter("archive", false);
		ArrayList<AnnonceDAO> listAnnonce =  (ArrayList<AnnonceDAO>) query.getResultList();
		s.close();
		return listAnnonce;
	}
}
