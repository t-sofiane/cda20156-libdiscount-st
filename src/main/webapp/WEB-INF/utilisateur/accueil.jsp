<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="topnav" >
  		<a class="active" href="RedirectionPages?param=home">Accueil</a>
  		<a href="RedirectionPages?param=poster">Poster</a>
 		<a href="ConsultAnnonce">Consulter</a>
  		<a href="AfficherMesAnnonce"> Gestion </a>
  		<a href="RedirectionPages?param=deconnect">Déconnexion</a>		
	</div>
	 
	<p align= "center"> Bonjour  <b> <c:out value="${utilisateur.nom}"/>  <c:out value="${utilisateur.prenom}"/> </b></p>
	<div id="footer">
		<p align=center>&copy; cda-2020  SOFIANE TAYEB</p>
	</div>
</body>
</html>