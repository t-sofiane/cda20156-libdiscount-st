<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>poster une annonce</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
		<div class="topnav">
	  		<a href="RedirectionPages?param=home">Accueil</a>
	  		<a class="active" href="RedirectionPages?param=poster">Poster</a>
 			<a href="ConsultAnnonce">Consulter</a>
	  		<a href="AfficherMesAnnonce">Gestion</a>
  			<a href="RedirectionPages?param=deconnect">D�connexion</a>		
	  		
		</div>
		<c:set var="idUser" value="${utilisateur.idUtilisateur}" scope="request" />
		<p>Bonjour  <c:out value="${utilisateur.nom}"/>  <c:out value="${utilisateur.prenom}"/> !</p>
		
		<form method="post" action="CreerAnnonce">
            <fieldset>
                <legend>Poster une annonce</legend>
				
				<label for="nom">Titre <span >*</span></label>
                <input type="text" id="titre" name="titre" value="titre"  required>
                <br />
                <label for="nom">Niveau scolaire <span >*</span></label>
                <input type="text" id="nivScolaire" name="nivScolaire" value="niveau Scolaire"  required>
                <br />
                <label for="email">ISBN </label>
                <input type="text" id="isbn" name="isbn" value="1010" >
                <br />
                <label for="email">Date d'edition </label>
                <input type="date" id="editDate" name="editDate" value="01/01/2020" >
                <br /> 
                <label for="motdepasse">Maison d'edition <span >*</span></label>
                <input type="text" id="maisonEdit" name="maisonEdit" value="maisonEdit">
                <br /> 
				<label for="nom">Prix Unitaire <span >*</span></label>
                <input type="text" id="prixUnit" name="prixUnit" value="10"  required>
                <br /> 
                <label for="nom">Quantit� <span >*</span></label>
                <input type="text" id="quantite" name="quantite" value="03"  required>
                
                <br /> 
               
                <input type="submit" value="Poster" class="sansLabel" />
            </fieldset>
        </form>
        <div id="footer">
		<p align=center>&copy; cda-2020  SOFIANE TAYEB</p>
	</div>
</body>
</html>