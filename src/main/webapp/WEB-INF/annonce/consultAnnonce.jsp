<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>consulter annonces</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="topnav" >
	  	<a href="RedirectionPages?param=home">Accueil</a>
  		<a href="RedirectionPages?param=poster">Poster</a>
 		<a class="active" href="ConsultAnnonce">Consulter</a>
  		<a href="AfficherMesAnnonce">Gestion</a>
  		<a href="RedirectionPages?param=deconnect">Déconnexion</a>		
  		
	</div>
	<p align= "center"> Bonjour  <b> <c:out value="${utilisateur.nom}"/>  <c:out value="${utilisateur.prenom}"/> </b></p>
	<form>
	  <div>
		<label for="type">selectionner</label>
		  <select name="type" id="type">
		    <option value="titre">titre</option>
		    <option value="niveau">niveau scolaire</option>
		    <option value="ville">ville</option>
		  </select>
	    <input type="search" id="recherche" name="recherche" placeholder="Rechercher">
	    <button>Rechercher</button>
	  
		</div>
		
	</form>
<c:forEach items="${listAnnonce }" var="annonce">
<br>
<div class="shadowbox" >
		<b>Titre :</b> <c:out value="${annonce.titreAnnonce }"/> <img src="images/book1.jpg" alt="" width="100" height="125" style="display:inline-block;
float:right;" >
		<br> 
		<b>Niveau Scolaire :</b> <c:out value="${annonce.nivScolaire }"/> <br> 
		<b>ISBN :</b> <c:out value="${annonce.isbn }"/> <br> 
		<b>Date Edition :</b> <c:out value="${annonce.dateEdition }"/> <br> 
		<b>Maison Edition :</b> <c:out value="${annonce.maisonEdition }"/> <br>
		<b>Prix Unitaire :</b> <c:out value="${annonce.prixUnit }"/> <br>
		<b>Quantite :</b> <c:out value="${annonce.quantite }"/> <br>
</div>
</c:forEach>
<div id="footer">
		<p align=center>&copy; cda-2020  SOFIANE TAYEB</p>
</div>
</body>
</html>